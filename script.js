let num1 = 2;
let cube = Math.pow(num1,4); 
console.log(`The cube of ${num1}  is ${cube}` );

const address = ['I live at 258 Washington Ave NW, Califonia 90011'];

const [myaddress] =  address;
console.log(`${myaddress}`);

const animal = {
    name: 'Lolong',
    species: 'salt crocodile',
    weight: '1075 kgs',
    measurement: '20 ft 3 in.'
}

const {name, species, weight, measurement} =  animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a
 measurement of ${measurement}  `);

 let arrayNumbers = [1,2,3,4,5]; 
 let i = 0;
 arrayNumbers.forEach((numbers) => {
    
    console.log(numbers)
     
 })

 let reduceNumber = arrayNumbers.reduce((previousValue, currentValue) => {
    return previousValue + currentValue
 })

 console.log(reduceNumber);


 class Dog {
  constructor(breed, name, age) {
    this.breed = breed,
    this.name = name,
    this.age = age
  }
}

const myDog = new Dog('German Shepherd', 'Tom', 5);
console.log(myDog);
myDog.breed = 'Golden Retriever';
myDog.name = 'Jaffet';
myDog.age = 5

console.log(myDog);
